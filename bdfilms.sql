drop table Film;
drop table Realisateur;


create table Realisateur(
IDR Number(4) primary key, 
NomRealisateur Varchar2(20), 
Age Number(4));


create table Film(
ID Number(4) primary key, 
NomFilm Varchar2(20),  
NomRealisateur VarChar2(20),
IDR Number(4),
Annee Date,
Genre Varchar(20),
constraint FKRealisateurFilm foreign key (IDR) references Realisateur(IDR) on delete cascade);


insert into Realisateur values(1000,'Toledano',42);
insert into Realisateur values(1001,'Besson',55);
insert into Realisateur values(1002,'Anderson',49);
insert into Realisateur values(1003,'Raimi',54);
insert into Realisateur values(1004,'Plaza',41);

insert into Film values(01,'Intouchables','Toledano',1000,to_date('02-01-2011','DD-MM-YYYY'),'Comedie');
insert into Film values(02,'Taxi','Besson',1001,to_date('02-10-1998','DD-MM-YYYY'),'Comedie');
insert into Film values(03,'Taxi2','Besson',1001,to_date('02-10-2000','DD-MM-YYYY'),'Comedie');
insert into Film values(04,'Taxi3','Besson',1001,to_date('02-10-2003','DD-MM-YYYY'),'Comedie');
insert into Film values(05,'Mortal Kombat','Anderson',1002,to_date('25-10-1995','DD-MM-YYYY'),'Fantastique');
insert into Film values(06,'Spider-man','Raimi',1003,to_date('12-06-2002','DD-MM-YYYY'),'Hero');
insert into Film values(07,'Spider-man2','Raimi',1003,to_date('14-07-2004','DD-MM-YYYY'),'Hero');
insert into Film values(08,'Resident evil','Anderson',1002,to_date('03-04-2002','DD-MM-YYYY'),'Horreur');
insert into Film values(09,'Rec','Plaza',1004,to_date('23-04-2008','DD-MM-YYYY'),'Horreur');



