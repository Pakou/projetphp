<?php

date_default_timezone_set('Europe/Paris');
try{
  // le fichier de BD s'appellera contacts.sqlite3
  $file_db=new PDO('sqlite:film.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	
  $file_db->exec("DROP table IF EXISTS film;
	CREATE TABLE film(
		id INTERGER PRIMARY KEY,
		NomFilm TEXT,
		NomRealisateur TEXT,
		idr INTEGER,
		annee DATE,
		genre TEXT)");
		  
	$film=array(
	array('id'=>'21',
		'NomFilm'=>'Intouchables',
		'NomRealisateur'=>'Toledano',
		'idr'=>'1000',
		'annee'=>'2011-01-02',
		'genre'=>'Comedie'),
	array('id'=>'22',
		'NomFilm'=>'Taxi',
		'NomRealisateur'=>'Besson',
		'idr'=>'1001',
		'annee'=>'1998-10-02',
		'genre'=>'Comedie'),
	array('id'=>'23',
		'NomFilm'=>'Taxi2',
		'NomRealisateur'=>'Besson',
		'idr'=>'1001',
		'annee'=>'2000-10-02',
		'genre'=>'Comedie'),
	array('id'=>'24',
		'NomFilm'=>'Taxi3',
		'NomRealisateur'=>'Besson',
		'idr'=>'1001',
		'annee'=>'2003-10-02',
		'genre'=>'Comedie'),
	array('id'=>'25',
		'NomFilm'=>'Mortal Kombat',
		'NomRealisateur'=>'Paul Anderson',
		'idr'=>'1002',
		'annee'=>'1995-10-25',
		'genre'=>'Fantastique'),
	array('id'=>'26',
		'NomFilm'=>'Spider-man',
		'NomRealisateur'=>'Raimi',
		'idr'=>'1003',
		'annee'=>'2002-06-12',
		'genre'=>'Hero'),
	array('id'=>'27',
		'NomFilm'=>'Spider-man2',
		'NomRealisateur'=>'Raimi',
		'idr'=>'1003',
		'annee'=>'2004-07-14',
		'genre'=>'Hero'),
	array('id'=>'28',
		'NomFilm'=>'Resident evil',
		'NomRealisateur'=>'Paul Anderson',
		'idr'=>'1002',
		'annee'=>'2002-04-03',
		'genre'=>'Horreur'),
	array('id'=>'29',
		'NomFilm'=>'Rec',
		'NomRealisateur'=>'Plaza',
		'idr'=>'1004',
		'annee'=>'2008-04-23',
		'genre'=>'Horreur'),
		);
		  
// Ajouter les années aux formats date.		  
		  
  $insert2="INSERT INTO film(id, NomFilm, NomRealisateur, idr, annee, genre) VALUES(:id, :NomFilm, :NomRealisateur, :idr, :annee, :genre)";
  $stmt2=$file_db->prepare($insert2);
  $stmt2->bindParam(':id',$id);
  $stmt2->bindParam(':NomFilm',$NomFilm);
  $stmt2->bindParam(':NomRealisateur',$NomRealisateur);
  $stmt2->bindParam(':idr',$idr);
  $stmt2->bindParam(':annee',$annee);
  $stmt2->bindParam(':genre',$genre);
  
  
  foreach ($film as $f){
    $id=$f['id'];
    $NomFilm=$f['NomFilm'];
    $NomRealisateur=$f['NomRealisateur'];
    $idr=$f['idr'];
    $annee=$f['annee'];
    $genre=$f['genre'];
    $stmt2->execute();
    echo $NomFilm ;            // Ici, on test pour savoir si notre table est bien implementée.
  }
  
  
 
  
  
  echo "Insertion en base reussie !";
  // on ferme la connexion
  $file_db=null;
  
}
catch(PDOException $ex){
  echo $ex->getMessage();
}
